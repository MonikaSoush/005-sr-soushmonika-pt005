import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import MyCard from "./components/MyCard";
import { Container, Row } from "react-bootstrap";
import MyTable from "./components/MyTable";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      food: [
        {
          id: 1,
          name: "Pizza",
          thumbnail: "/images/1.jpg",
          qty: 0,
          price: 5,
          total: 10,
        },
        {
          id: 2,
          name: "Chicken",
          thumbnail: "/images/2.jpg",
          qty: 0,
          price: 3,
          total: 4,
        },
        {
          id: 3,
          name: "Burger",
          thumbnail: "/images/3.jpg",
          qty: 0,
          price: 4,
          total: 1,
        },
        {
          id: 4,
          name: "Coca",
          thumbnail: "/images/4.jpg",
          qty: 0,
          price: 3.5,
          total: 2,
        },
      ],
    };
    
  }

  onAdd=(index)=>{
      
      console.log("index:", index);
      let temp=[...this.state.foods]
      temp[index].qty++
        temp[index].total=temp[index].qty*temp[index].price

      this.setState({
          foods:temp
      })
  }

  onDelete=(index)=>{
      
      console.log("index:", index);
      let temp=[...this.state.foods]
      temp[index].qty--
        temp[index].total=temp[index].qty*temp[index].price

      this.setState({
          foods:temp
      })
  }

  onClear=()=>{
    console.log("Clearing...");
    let foods=[...this.state.foods]
    // eslint-disable-next-line array-callback-return
    foods.map(item => {
        item.qty=0
        item.total=0
    })
    this.setState({
        foods
    })
  }

  render() {
    return (
      <Container>
        <Row>
          <MyCard 
          foods={this.state.foods} 
          onAdd={this.onAdd} 
          onDelete={this.onDelete} />
        </Row>
        <Row>
            <MyTable onClear={this.onClear} foods={this.state.foods}/>
        </Row>
      </Container>
    );
  }
}
