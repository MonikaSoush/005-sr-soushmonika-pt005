import React from "react";
import { Table,Button } from "react-bootstrap";

export default function MyTable ({foods, onClear}) {
  console.log("Props on func:", foods);

  let temp=foods.filter(item=>{
      return item.qty>0
  })

  return (
    <>
      <Button 
      onClick={onClear}
      variant="warning">Clear</Button>
      <h2>{temp.length} foods</h2>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>name</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {temp.map((item, index) => (
            <tr>
              <td></td>
              <td>{index + 1}</td>
              <td>{item.name}</td>
              <td>{item.qty}</td>
              <td>{item.price}</td>
              <td>{item.total}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}
